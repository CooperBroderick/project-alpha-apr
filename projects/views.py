# from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView

# Create your views here.


class ProjectsListView(LoginRequiredMixin, ListView):
    login_url = "/accounts/login/"
    model = Project
    template_name = "projects/home.html"
    context_object_name = "Project"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectsDetailView(LoginRequiredMixin, DetailView):
    login_url = "/accounts/login/"
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "Project"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectsCreateView(LoginRequiredMixin, CreateView):
    login_url = "/accounts/login/"
    model = Project
    template_name = "projects/create.html"
    context_object_name = "CreateProject"
    fields = ["name", "description", "members"]
    success_url = "/projects/1/"

    # def get_queryset(self):
    #     return Project.objects.filter(members=self.request.user)
