from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Project(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False)
    description = models.TextField(null=False, blank=False)
    members = models.ManyToManyField(
        User,
        related_name="projects",
    )

    def __str__(self):
        return self.name
