from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView
from tasks.models import Task

# Create your views here.


class CreateTaskView(LoginRequiredMixin, CreateView):
    login_url = "/accounts/login/"
    model = Task
    template_name = "tasks/create.html"
    context_object_name = "CreateTask"
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    success_url = "/projects/1/"


class ListTaskView(LoginRequiredMixin, ListView):
    login_url = "/accounts/login/"
    model = Task
    template_name = "/tasks/mine.html"
    context_object_name = "ListTasks"


class UpdateTaskView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
