from django.db import models
from projects.models import Project
from django.contrib.auth.models import User

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False)
    start_date = models.DateTimeField(
        auto_now=False, auto_now_add=False, null=False, blank=False
    )
    due_date = models.DateTimeField(
        auto_now=False, auto_now_add=False, null=False, blank=False
    )
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User,
        related_name="tasks",
        null=True,
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return self.name
